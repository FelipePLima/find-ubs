class BasicHealthUnit < ApplicationRecord
  has_one :score

  validates :cnes, :name, :longitude, :latitude, presence: true
  validates :cnes, uniqueness: true, case_sensitive: false

  reverse_geocoded_by :latitude, :longitude
end
