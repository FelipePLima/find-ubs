class Score < ApplicationRecord
  belongs_to :basic_health_unit

  def self.level(level)
    case level
    when 'Desempenho mediano ou  um pouco abaixo da média'
      1
    when 'Desempenho acima da média'
      2
    when 'Desempenho muito acima da média'
      3
    end
  end
end
