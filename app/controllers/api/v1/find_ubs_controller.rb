module Api
	module V1
		class FindUbsController < ApplicationController
			def index
				per_page = params[:per_page] ? params[:per_page] : 10
				@ubs = BasicHealthUnit.near(params[:query], 1).paginate(page: params[:page], per_page: per_page)

				render json: {current_page: params[:page].to_i,
											per_page: per_page.to_i,
											total_entries: BasicHealthUnit.all.size,
											entries: ActiveModel::Serializer::CollectionSerializer.new(@ubs, serializer: BasicHealthUnitSerializer)
                      }, status: :ok
      end
    end
	end
end
