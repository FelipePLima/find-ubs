class BasicHealthUnitSerializer < ActiveModel::Serializer
  attribute :cnes, key: :id
  attributes :name, :address, :city, :phone, :geocode

  has_one :score

  def geocode
    {longitude: object.longitude, latitude: object.latitude}
  end
end
