FactoryGirl.define do
  factory :score do
    size 3
    adaptation_for_seniors 3
    medical_equipment 1
    medicine 3
  end
end
