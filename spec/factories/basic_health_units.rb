FactoryGirl.define do
  factory :basic_health_unit do
    sequence :cnes do |n|
      "#{n}"
    end
    sequence :name do |n|
      "Posto de Saúde #{n}"
    end
    address "RUA BARAO MELGACO"
    city "São Paulo"
    phone "1137582329"
    longitude -46.7057347297655
    latitude -23.6099946498864
  end
end
