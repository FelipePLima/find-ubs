require 'rails_helper'

RSpec.describe Api::V1::FindUbsController, type: :controller do
  describe 'GET #index' do
    let!(:basic_health_unit) { create(:basic_health_unit, cnes: 2788470, name: 'UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO') }
    let!(:score) { create(:score, basic_health_unit_id: basic_health_unit.id) }

    context 'when find ubs near' do
      before do
        get :index,
            as: :json,
            params: { query: '-23.604936,-46.692999', page: 1, per_page: 1 }
      end

      it { expect(response).to be_successful }
      it 'when ubs are within the search area' do
        expect(response_json).to include_json(
          {"current_page":1,
           "per_page":1,
           "total_entries":1,
           "entries":[{
             "id":2788470,
             "name":"UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO",
             "address":"RUA BARAO MELGACO",
             "city":"São Paulo",
             "phone":"1137582329",
             "geocode":{
               "longitude":-46.7057347297655,
               "latitude":-23.6099946498864
            },
            "score":{
              "size":3,
              "adaptation_for_seniors":3,
              "medical_equipment":1,
              "medicine":3
            }
          }]
        }
        )
        expect(response_json['entries'].size).to eq(1)
      end
    end
  end
end
