require 'rails_helper'

RSpec.describe Score, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:basic_health_unit) }
  end

  describe '#level' do
    level1 = "Desempenho acima da média"
    level2 = "Desempenho muito acima da média"
    level3 = "Desempenho mediano ou  um pouco abaixo da média"

    it 'alter level score to integer' do
      expect(Score.level(level1)).to eq(2)
      expect(Score.level(level2)).to eq(3)
      expect(Score.level(level3)).to eq(1)
    end
  end
end
