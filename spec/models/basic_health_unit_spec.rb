require 'rails_helper'

RSpec.describe BasicHealthUnit, type: :model do
  describe 'associations' do
    it { is_expected.to have_one(:score) }
  end

  describe 'validations' do
    it { should validate_presence_of(:cnes) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:latitude) }
    it { should validate_presence_of(:longitude) }

    context 'with a persisted BasicHealthUnit' do
      subject { create(:basic_health_unit) }

      it do
        should validate_uniqueness_of(:cnes).case_insensitive
      end
    end
  end
end
