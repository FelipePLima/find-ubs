# README

Bem vindo a Api de busca de ubs no Brasil.

Com esse app você consegue fazer uma busca com a longitude e a latitude, das ubs mais próximas a essa localização.

Check list para o uso do app:

- baixe o projeto
- instale o postgresql
- rode o bundle install, para instalar as dependências do projeto
- configure seu database.yml
- rode rake db:create:all, para gerar os bancos de dados
- rode rake db:migrate, para gerar as tabelas que serão utilizadas no projeto
- adicionar o arquivo ubs.csv na pasta tmp do projeto (arquivo: http://dados.gov.br/dataset/unidades-basicas-de-saude-ubs)
- rode rails ubs:import, para importar as ubs no banco de dados
- rode rails s, para rodar o projeto, para acessar a api entre em localhost:sua_porta/api/v1/find_ubs?query=long,lat&page&per_page

qualquer duvida ou dificuldade entre em contato.
