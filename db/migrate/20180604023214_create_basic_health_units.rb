class CreateBasicHealthUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :basic_health_units do |t|
      t.integer :cnes
      t.string :name
      t.string :address
      t.string :city
      t.string :phone
      t.float :longitude
      t.float :latitude
      t.timestamps
    end
  end
end
