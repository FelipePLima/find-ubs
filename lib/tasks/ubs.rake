require 'csv'
namespace :ubs do
  desc "Importa o arquivo ubs.csv"
  task import: :environment do
    puts "Inicio da Importação"
    CSV.foreach('tmp/ubs.csv', col_sep: ',').with_index do |line, index|
     unless (index == 0)
       ubs = BasicHealthUnit.find_or_create_by(cnes: line[3], name: line[4], phone: line[8], address: line[5],
                                    city: line[7], longitude: line[1], latitude: line[0])
       Score.find_or_create_by(basic_health_unit: ubs, size: Score.level(line[9]), adaptation_for_seniors: Score.level(line[10]),
                    medical_equipment: Score.level(line[11]), medicine: Score.level(line[12]))
     end
    end
    puts "Fim da Importação"
  end
end
