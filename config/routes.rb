Rails.application.routes.draw do
  root to: 'api/v1/find_ubs#index'
  namespace 'api' do
  	namespace 'v1' do
  		resources :find_ubs, only: [:index]
  	end
  end
end
